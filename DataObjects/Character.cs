﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SRInit.DataObjects
{
    public class Character
    {
        public string name { get; set; }
        public Dice initDice { get; set; }
        public int initBonus { get; set; }
        public int initScore { get; set; }
        public bool hasActed { get; set; }

        public int rollInit()
        {
            initScore = initDice.roll() + initBonus;
            hasActed = false;
            return initScore;
        }

        public override string ToString()
        {
            return name + ": " + initScore;
        }
    }
}
