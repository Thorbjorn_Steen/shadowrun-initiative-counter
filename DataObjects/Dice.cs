﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SRInit.DataObjects
{
    public class Dice
    {
        public Random _randGen;

        public int NumDice;
        public int SizeDice;

        public Dice(int number, int size, Random randomGen)
        {
            _randGen = randomGen;

            NumDice = number;
            SizeDice = size;
        }

        public Dice(string dice, Random randomGen)
        {
            _randGen = randomGen;

            String[] parts = dice.Split('d');
            if (parts.Count() != 2)
            {
                throw new ArgumentException("'dice' argument must be in the format #d#");
            }
            NumDice = int.Parse(parts[0]);
            SizeDice = int.Parse(parts[1]);
        }

        public double averageRoll()
        {
            return (((double)SizeDice + 1.0) / 2.0) * (double)NumDice;
        }

        public int roll()
        {
            var result = 0;
            for (var i = 0; i < NumDice; i++)
            {
                result += _randGen.Next(1, SizeDice + 1);
            }
            return result;
        }

        public override string ToString()
        {
            return NumDice + "d" + SizeDice;
        }
    }
}
