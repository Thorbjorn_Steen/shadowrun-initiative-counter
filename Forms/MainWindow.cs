﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SRInit.DataObjects;
using SRInit.Forms;

namespace SRInit
{
    public partial class MainWindow : Form
    {
        private Random _universalRandomGen;

        private List<Character> _removedCharacters;
        private int _combatPhase;

        public MainWindow()
        {
            InitializeComponent();

            _universalRandomGen = new Random();

            _removedCharacters = new List<Character>();
            _combatPhase = 1;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var dialog = new AddDialog(this, _universalRandomGen);
            var character = dialog.Open();
        }

        public void addCharacter(Character character)
        {
            lstInitiative.Items.Add(character);
            resortListview();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            removeCharacter((Character) lstInitiative.SelectedItem);
        }

        public void removeCharacter(Character character)
        {
            lstInitiative.Items.Remove(character);
        }

        private void btnAdvance_Click(object sender, EventArgs e)
        {
            var activeCharacter = (Character) lstInitiative.Items[0];

            //Check whether the active character has enough initiative to join next action phase
            if (activeCharacter.initScore < 11)
            {
                _removedCharacters.Add(activeCharacter);
                removeCharacter(activeCharacter);
            }
            else
            {
                removeCharacter(activeCharacter);
                activeCharacter.initScore -= 10;
                addCharacter(activeCharacter);
                activeCharacter.hasActed = true;
                resortListview();
            }

            //If there are no items left, we reroll
            if (lstInitiative.Items.Count == 0)
            {
                reroll();
                return;
            }

            //If a character that has acted ends up in first place, we are starting a new action phase
            var newActiveCharacter = (Character)lstInitiative.Items[0];
            if (newActiveCharacter.hasActed)
            {
                foreach (Character character in lstInitiative.Items)
                {
                    character.hasActed = false;
                }
                _combatPhase++;
                updateCombatPhaseLabel();
            }
        }

        private void btnReroll_Click(object sender, EventArgs e)
        {
            reroll();
        }

        private void reroll()
        {
            var allCharacters = new List<Character>(_removedCharacters);
            allCharacters.AddRange(lstInitiative.Items.Cast<Character>());

            lstInitiative.Items.Clear();
            _removedCharacters.Clear();
            _combatPhase = 1;
            updateCombatPhaseLabel();

            foreach (var character in allCharacters)
            {
                character.rollInit();
                lstInitiative.Items.Add(character);
            }
            resortListview();
        }

        private void resortListview()
        {
            lstInitiative.Sorted = false;
            lstInitiative.Sorted = true;
        }

        private void updateCombatPhaseLabel()
        {
            lblCombatPhase.Text = "Combat Phase: " + _combatPhase;
        }
    }
}
