﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SRInit.DataObjects;

namespace SRInit
{
    public partial class AddDialog : Form
    {
        private Random _universalRandomGen;

        private Character _character;
        private MainWindow _mainWindow;


        public AddDialog(MainWindow mainWindow, Random universalRandomGen)
        {
            InitializeComponent();
            
            _mainWindow = mainWindow;
            _universalRandomGen = universalRandomGen;

            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.MinimizeBox = false;
            this.MaximizeBox = false;

            cmbInitDice.Items.AddRange(new object[]{"1d6","2d6","3d6","4d6","5d6"});
            cmbInitDice.SelectedIndex = 0;
        }

        public Character Open()
        {
            ShowDialog();
            return _character;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            addCharacter();
            txtName.Text = "";
            txtInitBonus.Text = "0";
            cmbInitDice.SelectedIndex = 0;
            txtInitScore.Text = "";
            txtName.Focus();
        }

        private void addCharacter()
        {
            _character = new Character();
            _character.name = txtName.Text;
            _character.initBonus = Convert.ToInt32(txtInitBonus.Text);
            _character.initDice = new Dice(cmbInitDice.SelectedItem.ToString(), _universalRandomGen);
            _character.hasActed = false;
            if (txtInitScore.Text.Equals(""))
            {
                _character.rollInit();
            }
            else
            {
                _character.initScore = Convert.ToInt32(txtInitScore.Text);
            }
            _mainWindow.addCharacter(_character);
        }

        private void btnAddAndClose_Click(object sender, EventArgs e)
        {
            addCharacter();
            Close();
        }

        private void txtInitBonus_Enter(object sender, EventArgs e)
        {
            txtInitBonus.SelectAll();
        }

        private void txtInitBonus_MouseClick(object sender, MouseEventArgs e)
        {
            txtInitBonus.SelectAll();
        }
    }
}
