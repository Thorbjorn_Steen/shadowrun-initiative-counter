﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SRInit.DataObjects;

namespace SRInit.Forms
{
    public class MySortedListBox : ListBox
    {
        private bool _swapped;
        private int _sortCounter;

        public MySortedListBox() : base()
        {
            Sorted = true;
        }

        protected override void Sort()
        {
            if (Items.Count > 1)
            {
                do
                {
                    _sortCounter = Items.Count - 1;
                    _swapped = false;
                    while (_sortCounter > 0)
                    {
                        Character char1 = (Character)Items[_sortCounter];
                        Character char2 = (Character)Items[_sortCounter - 1];
                        if (!char1.hasActed && char2.hasActed)
                        {
                            swap();
                        }
                        else if ((char1.initScore-1) % 10 > (char2.initScore-1) % 10 && !(char1.hasActed && !char2.hasActed))
                        {
                            swap();
                        }

                        _sortCounter -= 1;
                    }
                } while (_swapped);
            }
        }

        private void swap()
        {
            object temp = Items[_sortCounter];
            Items[_sortCounter] = Items[_sortCounter - 1];
            Items[_sortCounter - 1] = temp;
            _swapped = true;
        }
    }
}
